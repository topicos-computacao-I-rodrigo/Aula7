import { Injectable } from "../../node_modules/@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class ContatoProvider {
  constructor(private storage: Storage) { }

  listContatos(): Promise<Contato[]> {
    return this.storage.get('contatos')
  }

  addContato(contato: Contato) {
    return this.storage.get('contatos').then((contatos: Contato[]) => {
      if (contatos) {
        contatos.push(contato);
        this.storage.set('contatos', contatos);
      } else {
        this.storage.set('contatos', [contato]);
      }
    });
  }
}

export class Contato {
  nome: string
  tel: string
}