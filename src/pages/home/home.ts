import { MapaPage } from './../mapa/mapa';
import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StoragePage } from '../storage/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mapa: any;
  storage: any;

  constructor () {
    this.mapa = MapaPage;
    this.storage = StoragePage;
  }
}
