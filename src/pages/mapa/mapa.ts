import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import leaflet from 'leaflet';

@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage implements OnInit {
  map: any;

  constructor(private geolocation: Geolocation) { }

  ngOnInit() {
    this.map = new leaflet.map('map').fitWorld();

    leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);
  }

  locate() {
    this.geolocation.getCurrentPosition().then((resp) => {
      let lat = resp.coords.latitude;
      let lng = resp.coords.longitude;

      let marker = new leaflet.marker([lat, lng]).addTo(this.map)
        .bindPopup('Estou Aqui!');
      
      let bounds = leaflet.latLngBounds([marker.getLatLng()]);
      this.map.flyToBounds(bounds);
    });
  }

}
