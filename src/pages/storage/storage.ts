import { ContatoProvider, Contato } from './../../providers/contato';
import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-storage',
  templateUrl: 'storage.html',
  providers: [ContatoProvider]
})
export class StoragePage {
  contatos: Contato[]

  constructor(public alertCtrl: AlertController, private contatoProvider: ContatoProvider) { }

  ionViewDidLoad() {
    this.contatoProvider.listContatos().then((contatos: Contato[]) => {
      this.contatos = contatos;
    });
  }

  addContato() {
    const alert = this.alertCtrl.create({
      title: 'Add Contato',
      message: "Preencha o nome e o telefone",
      inputs: [
        {
          name: 'nome',
          placeholder: 'Nome'
        },
        {
          name: 'tel',
          placeholder: 'Tel',
          type: 'tel'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Salvar',
          handler: (contato: Contato) => {
            this.contatoProvider.addContato(contato).then(() => {
              this.contatoProvider.listContatos().then((contatos: Contato[]) => {
                this.contatos = contatos;
              });
            });
          }
        }
      ]
    });

    alert.present();
  }

}
